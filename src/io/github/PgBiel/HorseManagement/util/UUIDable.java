package io.github.PgBiel.HorseManagement.util;

import java.util.UUID;

/** Anything that can have an UUID. */
public interface UUIDable {
    UUID getUniqueId();
}
