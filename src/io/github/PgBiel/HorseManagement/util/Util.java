package io.github.PgBiel.HorseManagement.util;

/* NOTE: In here I use a lot of reflection for compatibility so I do not have to use
 * deprecated methods. This, however, costed some readability. :(
 */

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.github.PgBiel.HorseManagement.menus.holders.MenuHolder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Server;
import org.bukkit.block.Container;
import org.bukkit.entity.Entity;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

import io.github.PgBiel.HorseManagement.HorseLogger;
import io.github.PgBiel.HorseManagement.HorseManagement;

public class Util { // most of this can be moved to PgCommonsLib

	/** Item used to claim and manage horses. **/
	private static ItemStack horseOwn;

	/** Server's Minecraft version. */
	private static final String version = Bukkit.getVersion();

	/** The server object. */
	private static final Server server = Bukkit.getServer();

	/** If MC Version of the Server is under or equal to 1.8 */
	private static boolean isOld; // if MC version is under 1.9

	/** If MC Version of the Server is under 1.14 */
	private static boolean isOld114; // if MC version is under 1.14

	/** The plugin instance. */
	private static HorseManagement plugin;

	static {
		// MAKING OUR COOL Claiming and Management ITEM!
		ItemStack book = new ItemStack(Material.BOOK);
		ItemMeta meta = book.getItemMeta();
		meta.setDisplayName("§a§b§c§r§r§aHorse Management");
		List<String> lore = new ArrayList<String>();
		lore.add("§r§7Hit a horse with this to open");
		lore.add("§r§7the management menu!");
		meta.setLore(lore);
		book.setItemMeta(meta);
		horseOwn = book;
		
		Pattern p = Pattern.compile("(\\d+)\\.(\\d+)(?:\\.\\d+)?-?"); // match version digits a.b.c
		Matcher m = p.matcher(version); // ^
		
		if (m.find()) {
			int firstV = Integer.parseInt(m.group(1));
			int secondV = Integer.parseInt(m.group(2));
			if (firstV < 1 || (firstV == 1 && secondV < 9)) { // under 1.9 - isOld yes, isOld114 yes
				isOld = true;
				isOld114 = true;
			} else if (firstV == 1 && secondV < 14) { // under 1.14 - isOld nope, isOld114 yes
				isOld = false;
				isOld114 = true;
			} else { // 1.14 or above
				isOld = false;
				isOld114 = false;
			}
		} else { // something went wrong; enabling max compatibility
			isOld = true;
			isOld114 = true;
			HorseLogger.warn("Version did not match regex. Fallbacking to 'is old'.\n" +
			"Implementation: " + version);
		}
	}

	/**
	 * Sets the plugin var.
	 * @param plug The plugin instance
	 */
	public static void setPlugin(HorseManagement plug) {
		plugin = plug;
	}

	/**
	 * Turn ampersand colors into true colors (\& -> §), in order to manage color typing in players'
	 * chats (For commands etc.)
	 * @param text Text to convert colors at.
	 * @return New text with converted colors.
	 */
	public static String processColors(String text) {
		Pattern p = Pattern.compile("&([abcdefmnorkl\\d])");
		
		Matcher m = p.matcher(text);
		return m.replaceAll("§$1");
	}

	/**
	 * Check if an array contains a string, ignoring case.
	 * @param arr String array to check.
	 * @param test String to test for, case-insensitively.
	 * @return Whether or not the string is case-insensitively present in the `arr` array.
	 */
	public static boolean arrayContainsIns(String[] arr, String test) {
		for (String el : arr) {
			if (el.equalsIgnoreCase(test)) return true;
		}
		return false;
	}

	/**
	 * Repeat a string N times.
	 * @param str String to repeat
	 * @param amount "N" times; amount of times to repeat.
	 * @return The new string, result of repetition.
	 */
	public static String repeatStr(String str, int amount) {
		String repeated = new String(new char[amount]).replace("\0", str);
		return repeated;
	}

	/**
	 * Split a string into multiple pages, depending on the max amount of characters per line and dividing by words.
	 * @param input Input big string.
	 * @param maxCharInLine Max amount of chars every line.
	 * @return Array of pages (strings).
	 */
	public static String[] splitPages(String input, int maxCharInLine) {

	    StringTokenizer tok = new StringTokenizer(input, " ");
	    StringBuilder output = new StringBuilder(input.length()); // the output
	    int lineLen = 0;
	    while (tok.hasMoreTokens()) {
	        String word = tok.nextToken();

	        while (word.length() > maxCharInLine) {
	            output.append(word.substring(0, maxCharInLine - lineLen) + "\n");
	            word = word.substring(maxCharInLine - lineLen);
	            lineLen = 0;
	        }

	        if (lineLen + word.length() > maxCharInLine) {
	            output.append("\n");
	            lineLen = 0;
	        }
	        output.append(word + " ");

	        lineLen += word.length() + 1;
	    }
	    
	    return output.toString().split("\n");
	}

	/**
	 * Checks if the server version is 1.8.x or under.
	 * @return Whether the server is older than 1.9
	 */
	public static boolean isOld() {
		return isOld;
	}

	/**
	 * Checks if the server version is 1.13.x or under.
	 * @return Whether the server is older than 1.14
	 */
	public static boolean isOld114() { return isOld114; }

	/**
	 * Get the item in the player's main hand.
	 * Note that this method just exists for 1.8 compatibility.
	 * @param p Player to get item in Main Hand.
	 * @return The item in main hand, if any.
	 */
	public static ItemStack getMainHand(Player p) {
		Method method = null;
		Class<PlayerInventory> clazz = PlayerInventory.class;
		PlayerInventory inv = p.getInventory();
		try {
			if (isOld) {
				method = clazz.getMethod("getItemInHand"); // under 1.8 there was no left hand.
			} else {
				method = clazz.getMethod("getItemInMainHand");
			}
		} catch (NoSuchMethodException |
				SecurityException e) {
			HorseLogger.sev("Exception on getting main hand method!");
			e.printStackTrace();
			return null;
		}
		try {
			return (ItemStack) method.invoke(inv);
		} catch (IllegalAccessException | 
				IllegalArgumentException | 
				InvocationTargetException e) {
			HorseLogger.sev("Exception on running main hand method!");
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Make entity 2 ride entity 1.
	 * Note that this class is only here for the compatibility support added, otherwise we would just use
	 * Entity#addPassenger.
	 * @param e Entity to be ridden.
	 * @param e2 Entity that rides.
	 * @return Whether (true) or not (false) riding was successful.
	 */
	public static boolean ride(Entity e, Entity e2) { // For multiple entities limit to new method
		Method method = null;
		Class<Entity> clazz = Entity.class;
		try {
			if (isOld) {
				method = clazz.getMethod("setPassenger", Entity.class);
			} else {
				method = clazz.getMethod("addPassenger", Entity.class);
			}
		} catch (NoSuchMethodException | SecurityException err) {
			HorseLogger.sev("Exception on getting passenger method!");
			err.printStackTrace();
			return false;
		}
		
		try {
			return (boolean) method.invoke(e, e2);
		} catch (IllegalAccessException |
				IllegalArgumentException |
				InvocationTargetException err) {
			HorseLogger.sev("Exception on running passenger method!");
			err.printStackTrace();
			return false;
		}
	}

	/**
	 * Check if a potion has a "bad," "negative" effect on players.
	 * @param potion Potion to check.
	 * @return Whether (true) or not (false) the potion is "bad" or "negative."
	 */
	public static boolean isBadPotion(ItemStack potion) { // compatible with 1.8 I guess
		Material type = potion.getType();
		if (!type.equals(Material.POTION) && !type.equals(Material.SPLASH_POTION) &&
				(isOld ? true : !type.equals(Material.valueOf("LINGERING_POTION")))) return false;
		List<PotionEffectType> baddies = Arrays.asList(PotionEffectType.SLOW, 
				PotionEffectType.SLOW_DIGGING, PotionEffectType.HARM, PotionEffectType.CONFUSION, 
				PotionEffectType.INVISIBILITY, PotionEffectType.BLINDNESS, 
				PotionEffectType.HUNGER, PotionEffectType.WEAKNESS, PotionEffectType.POISON, 
				PotionEffectType.WITHER, isOld ? PotionEffectType.SLOW : PotionEffectType
						.getByName("UNLUCK"));
		try {
			if (isOld) { // versions under 1.9 - non-existent methods
				Class<?> clazz;
				Collection<?> effects = null;
				clazz = Class.forName("org.bukkit.potion.Potion");
				Method fromItemStack = clazz.getMethod("fromItemStack", ItemStack.class);
				Object potionObj = fromItemStack.invoke(null, potion);
				Method getEffects = clazz.getMethod("getEffects");
				Object semiEffects = getEffects.invoke(potionObj);
				effects = (Collection<?>) semiEffects;
				for (Object eff : effects) {
					PotionEffect effect = (PotionEffect) eff;
					if (baddies.contains(effect.getType())) return true;
				}
				Method getType = clazz.getMethod("getType");
				PotionType pType = (PotionType) getType.invoke(potionObj);
				PotionEffectType effType = pType.getEffectType();
				if (baddies.contains(effType)) return true;
			} else { // 1.9+
				Class <?> clazz;
				clazz = Class.forName("org.bukkit.inventory.meta.PotionMeta");
				Method hasEffect = clazz.getMethod("hasCustomEffect", PotionEffectType.class);
				ItemMeta meta = potion.getItemMeta();
				Object potionMeta = clazz.cast(meta);
				for (PotionEffectType baddie : baddies) {
					if ((Boolean) hasEffect.invoke(potionMeta, baddie)) return true;
				}
				Method getData = clazz.getMethod("getBasePotionData");
				clazz = Class.forName("org.bukkit.potion.PotionData");
				Object potionData = getData.invoke(potionMeta);
				Method getType = clazz.getMethod("getType");
				PotionType pType = (PotionType) getType.invoke(potionData);
				PotionEffectType effType = pType.getEffectType();
				if (baddies.contains(effType)) return true;
			}
		} catch (ClassNotFoundException |
				IllegalAccessException |
				NoSuchMethodException | 
				IllegalArgumentException | 
				InvocationTargetException e) {
			HorseLogger.sev("Exception on checking if potion is bad!");
			e.printStackTrace();
			return false;
		}
		return false;
	}

	/**
	 * Returns the item necessary to claim or manage a horse (by hitting it).
	 * @return The claiming and managing item
	 */
	public static ItemStack getHorseOwnItem() { // I did this because I use this in 2+ places
		return horseOwn;
	} // the magic book that claims horses

	/**
	 * Check if a player has any bypass at all.
	 * @param p Player to check.
	 * @param bypass Bypass type to check.
	 * @return Whether (true) or not (false) the player has such bypass.
	 */
	public static boolean bypass(Player p, String bypass) { // TODO: Add UUID support
		return !plugin.getPlayerBypass(p) && p.hasPermission("horsemng.bypass." + bypass);
	}

	/**
	 * Check if a player has a specific bypass (e.g. bypass claimed horses' damage immunity).
	 * @param p Player to check.
	 * @param bypass Bypass type to check.
	 * @return Whether (true) or not (false) the player has such bypass.
	 */
	public static boolean checkOn(Player p, String bypass) { // TODO: Add UUID support
		return plugin.getConfig().getBoolean("checks." + bypass) && !bypass(p, bypass);
	}

	/**
	 * Get an offline player's name, based on UUID. Used for checking horse ownership, hence the reason it checks whether
	 * the player has played before.
	 * @param uuid UUID of player to check.
	 * @return The name of the player or "&lt;unknown&gt;" if none.
	 */
	public static String ownerName(UUID uuid) {
		OfflinePlayer offp = server.getOfflinePlayer(uuid);
		if (offp.hasPlayedBefore()) {
			return offp.getName();
		} else {
			return "<unknown>";
		}
	}

	/**
	 * Get an inventory's title, if possible.
	 * @param inv Inventory to obtain info from.
	 * @return Inventory's title, if possible to obtain; otherwise, null.
	 */
	public static String invName(Inventory inv) {
		InventoryHolder holder = inv.getHolder();
		if (holder instanceof MenuHolder) {
			return ((MenuHolder) holder).getID();
		} else if (holder instanceof Container) {
			return ((Container) holder).getCustomName();
		} else if (holder instanceof HumanEntity) {
			return ((HumanEntity) holder).getOpenInventory().getTitle();
		}
		return null;
	}
}
