package io.github.PgBiel.HorseManagement;

import java.util.List;
import java.util.UUID;

import io.github.PgBiel.HorseManagement.menus.Menu;
import io.github.PgBiel.HorseManagement.menus.holders.MenuHolder;
import io.github.PgBiel.HorseManagement.util.UUIDable;
import org.bukkit.Material;
// import org.bukkit.Server;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityUnleashEvent;
import org.bukkit.event.entity.PlayerLeashEntityEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerUnleashEntityEvent;
import org.bukkit.event.vehicle.VehicleEnterEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.projectiles.ProjectileSource;

import io.github.PgBiel.HorseManagement.menus.OnClickable;
import io.github.PgBiel.HorseManagement.menus.ClaimHorseMenu;
import io.github.PgBiel.HorseManagement.menus.HorseManageMenu;
import io.github.PgBiel.HorseManagement.util.Util;

/**
 * Listens to all events.
 */
public class HorseListener implements Listener {

	/**
	 * The plugin instance.
	 */
	private static HorseManagement plugin;
	// private static Server server;

	/**
	 * The list of valid Menus.
	 */
	private Menu[] menus;

	/**
	 * Setup the listeners.
	 * @param plug The plugin instance to be used.
	 */
	protected HorseListener(HorseManagement plug) {
		plugin = plug;
		// server = plug.getServer();
		menus = new Menu[]{ new ClaimHorseMenu(plugin), new HorseManageMenu(plugin) };
	}

	/**
	 * This listener, Entity Damage Entity Event, is used to check when a player hits a claimed horse and if that horse
	 * is theirs, in order to prevent people hitting others' claimed horses.
	 * @param event The event object to work with.
	 */
	@EventHandler
	public void onEntityDamageByEntityEvent(EntityDamageByEntityEvent event) {
		Entity damager = event.getDamager();
		if ((!(damager instanceof Player) && !(damager instanceof Projectile)) ||
				!(event.getEntity() instanceof Horse)) return;
		Horse horse = (Horse) event.getEntity();
		YamlConfiguration horseData = plugin.getHorseData();
		final boolean checkOn = plugin.getConfig().getBoolean("checks.damage");
		String uid = horse.getUniqueId().toString();
		final boolean hasUid = horseData.contains(uid);
		final boolean cancel = hasUid && !event.isCancelled() && checkOn;
		if (damager instanceof Player) {
			Player player = (Player) event.getDamager();
			ItemStack book = Util.getHorseOwnItem();
			if (Util.getMainHand(player).isSimilar(book)) {
				event.setCancelled(true);
				if (!(horse.isTamed())) {
					player.sendMessage("§cYou can only use the Horse Management Menu on tamed "
							+ "horses!");
					return;
				}
				if (hasUid) {
					ConfigurationSection data = horseData.getConfigurationSection(uid);
					String uuid = data.getString("owner");
					if (uuid.equals(player.getUniqueId().toString())) {
						new HorseManageMenu(plugin).setupPlayer(player, horse);
					} else {
						player.sendMessage("§c§lYo!§c That's not your horse!");
					}
				} else {
					new ClaimHorseMenu(plugin).setupPlayer(player, horse);
				}
			} else if (cancel && !Util.bypass(player, "damage")) {
				ConfigurationSection data = horseData.getConfigurationSection(uid);
				String owner = data.getString("owner");
				UUID uuid;
				try {
					uuid = UUID.fromString(owner);
				} catch (IllegalArgumentException e) {
					HorseLogger.warn("Owner field of horse " + uid + ", that is " + owner 
							+ ", was an invalid UUID.");
					return;
				}
				if (!uuid.equals(player.getUniqueId())) {
					event.setCancelled(true);
					String name = Util.ownerName(uuid);
					player.sendMessage("§c§lYo! §cYou may not damage " + name + "'s horse.");
				}
			}
		} else if (damager instanceof Projectile && cancel) {
			Projectile proj = (Projectile) damager;
			ProjectileSource source = proj.getShooter();
			if (source instanceof Player) {
				Player player = (Player) source;
				if (Util.bypass(player, "damage")) return;
				ConfigurationSection data = horseData.getConfigurationSection(uid);
				UUID uuid = UUID.fromString(data.getString("owner"));
				if (!uuid.equals(player.getUniqueId())) {
					event.setCancelled(true);
					String name = Util.ownerName(uuid);
					player.sendMessage("§c§lYo! §cYou may not damage " + name + "'s horse.");
				}
			}
		}
	}

	/**
	 * Listener used for delegating clicks in GUIs to their respective Menu objects.
	 * @param event Event object to work with.
	 */
	@EventHandler
	public void onInventoryClickEvent(InventoryClickEvent event) {
		if (event.getCurrentItem() == null ||
				event.getCurrentItem().getType().equals(Material.AIR)) return;
		for (Menu menu : menus) {
			// if it's a different menu, pass
			if (!(event.getView().getTitle()).equalsIgnoreCase(menu.getMenuTitle())) continue;
			event.setCancelled(true);
			menu.onClick(event);
			break;
		}
	}

	/**
	 * Prevent people mounting in others' claimed horses.
	 * @param event Event object to work with.
	 */
	@EventHandler
	public void onVehicleEnterEvent(VehicleEnterEvent event) {
		if (!(event.getEntered() instanceof Player) ||
				!(event.getVehicle() instanceof Horse) ||
				event.isCancelled()) return;
		Player player = (Player) event.getEntered();
		if (!Util.checkOn(player, "mount")) return;
		Horse horse = (Horse) event.getVehicle();
		String uid = horse.getUniqueId().toString();
		YamlConfiguration horseData = plugin.getHorseData();
		if (horseData.contains(uid)) { // Someone is tryna mount on our horse!
			ConfigurationSection section = horseData.getConfigurationSection(uid);
			if (section == null) return;
			String uuid = player.getUniqueId().toString();
			String owner = section.getString("owner");
			if (!uuid.equals(owner)) {
				List<String> friends = section.getStringList("friends");
				if (!friends.contains(uuid)) {
					event.setCancelled(true);
					String name = Util.ownerName(UUID.fromString(owner));
					player.sendMessage("§c§lYo! §cYou may not mount on " + name + "'s horse.");
				}
			}
		}
	}

	/**
	 * Remove horse from config if it dies, in order to not overpopulate the config.
	 * @param event The event to work with.
	 */
	@EventHandler
	public void onEntityDeathEvent(EntityDeathEvent event) {
		if (!(event.getEntity() instanceof Horse)) return;
		Horse horse = (Horse) event.getEntity();
		String uid = horse.getUniqueId().toString();
		YamlConfiguration horseData = plugin.getHorseData();
		if (horseData.contains(uid)) {
			horseData.set(uid, null); // Our horse died, gotta remove from config.
		}
	}

	/**
	 * Prevent players from messing with others' claimed horses' inventory and armor.
	 * @param event The event object to work with.
	 */
	@EventHandler
	public void onPlayerInteractEntityEvent(PlayerInteractEntityEvent event) {
		if (!(event.getRightClicked() instanceof Horse) || event.isCancelled()) return;
		Player player = event.getPlayer();
		if (!Util.checkOn(player, "interact")) return;
		Horse horse = (Horse) event.getRightClicked();
		if (player.isSneaking()) {
			YamlConfiguration horseData = plugin.getHorseData();
			String uid = horse.getUniqueId().toString();
			if (horseData.contains(uid)) { // Someone is tryna modify our horse!
				ConfigurationSection section = horseData.getConfigurationSection(uid);
				String owner = section.getString("owner");
				if (!player.getUniqueId().toString().equals(owner)) {
					event.setCancelled(true);
					String name = Util.ownerName(UUID.fromString(owner));
					player.sendMessage("§c§lYo! §cYou may not interact with " + name + "'s "
							+ "horse.");
				}
			}
		}
	}

	/**
	 * Prevent players from leashing others' claimed horses.
	 * @param event The event object to work with.
	 */
	@EventHandler
	public void onPlayerLeashEntityEvent(PlayerLeashEntityEvent event) {
		if (!(event.getEntity() instanceof Horse) || event.isCancelled()) return;
		Player player = event.getPlayer();
		if (!Util.checkOn(player, "leash")) return;
		Horse horse = (Horse) event.getEntity();
		YamlConfiguration horseData = plugin.getHorseData();
		String uid = horse.getUniqueId().toString();
		if (horseData.contains(uid)) { // Someone is tryna leash our horse!
			ConfigurationSection section = horseData.getConfigurationSection(uid);
			String owner = section.getString("owner");
			if (!owner.equals(player.getUniqueId().toString())) {
				event.setCancelled(true);
				String name = Util.ownerName(UUID.fromString(owner));
				player.sendMessage("§c§lYo! §cYou may not leash " + name + "'s "
							+ "horse.");
			}
		}
	}

	/**
	 * Prevent players from unleashing others' claimed horses.
	 * @param event The event object to work with.
	 */
	@EventHandler
	public void onPlayerUnleashEntityEvent(PlayerUnleashEntityEvent event) {
		if (!(event.getEntity() instanceof Horse) ||
				event.isCancelled() ||
				!event.getReason().equals(EntityUnleashEvent.UnleashReason.PLAYER_UNLEASH)
				) return;
		Player player = event.getPlayer();
		if (!Util.checkOn(player, "unleash")) return;
		Horse horse = (Horse) event.getEntity();
		YamlConfiguration horseData = plugin.getHorseData();
		String uid = horse.getUniqueId().toString();
		if (horseData.contains(uid)) { // Someone is tryna unleash our horse!
			ConfigurationSection section = horseData.getConfigurationSection(uid);
			String owner = section.getString("owner");
			if (!owner.equals(player.getUniqueId().toString())) {
				event.setCancelled(true);
				String name = Util.ownerName(UUID.fromString(owner));
				player.sendMessage("§c§lYo! §cYou may not unleash " + name + "'s "
							+ "horse.");
			}
		}
	}

	/**
	 * Prevent players from applying effects to others' claimed horses.
	 * @param event The event object to work with.
	 */
	@EventHandler
	public void onPotionSplashEvent(PotionSplashEvent event) {
		ThrownPotion potion = event.getEntity();
		ProjectileSource source = potion.getShooter();
		if (!(source instanceof Player) ||
				event.isCancelled() ||
				!Util.isBadPotion(potion.getItem())) return;
		Player player = (Player) source;
		if (!Util.checkOn(player, "potion")) return;
		YamlConfiguration horseData = plugin.getHorseData();
		for (Entity ent : event.getAffectedEntities()) {
			if (ent instanceof Horse) {
				Horse horse = (Horse) ent;
				String uid = horse.getUniqueId().toString();
				if (horseData.contains(uid)) {
					ConfigurationSection section = horseData.getConfigurationSection(uid);
					String owner = section.getString("owner");
					if (!owner.equals(player.getUniqueId().toString())) {
						event.setCancelled(true);
						String name = Util.ownerName(UUID.fromString(owner));
						player.sendMessage("§c§lYo! §cYou may not give bad effects to " + name 
								+ "'s horse.");
					}
				}
			}
		}
	}
}
