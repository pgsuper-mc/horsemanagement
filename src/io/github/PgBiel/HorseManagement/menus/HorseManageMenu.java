package io.github.PgBiel.HorseManagement.menus;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import io.github.PgBiel.HorseManagement.menus.holders.MenuHolder;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import io.github.PgBiel.HorseManagement.HorseLogger;
import io.github.PgBiel.HorseManagement.HorseManagement;
import io.github.PgBiel.HorseManagement.menus.Menu;
import io.github.PgBiel.HorseManagement.util.Util;

/** Menu for managing the horses (unclaim horse, select it or set who can ride it). */
public class HorseManageMenu extends Menu {

	/** Temporary map between players and the horse they are managing. */
	private static Map<UUID, Horse> horses = new HashMap<UUID, Horse>();

	/** Inventory items. */
	private static ItemStack unclaim, select, ride;

	/** The menu title. */
	protected static final String title = "§6§lHorse Management";

	static {
		unclaim = new ItemStack(Material.ENDER_CHEST);
		select = new ItemStack(Material.BOW);
		ride = new ItemStack(Material.SADDLE);
		
		ItemMeta unclaimMeta = unclaim.getItemMeta();
		ItemMeta selectMeta = select.getItemMeta();
		ItemMeta rideMeta = ride.getItemMeta();
		
		unclaimMeta.setDisplayName("§cUnclaim horse");
		selectMeta.setDisplayName("§5Select horse");
		rideMeta.setDisplayName("§6Ride horse");
		
		unclaimMeta.setLore(Arrays.asList("§7Let someone else", "§7do it."));
		selectMeta.setLore(Arrays.asList("§7Use this with", "§b/rider§7, §b/horsename§7,", 
				"§b/horsetp§7."));
		rideMeta.setLore(Arrays.asList("§7Wee-hoo! A quick", "§7shortcut."));
		
		unclaim.setItemMeta(unclaimMeta);
		select.setItemMeta(selectMeta);
		ride.setItemMeta(rideMeta);
	}

	/** Initiate the menu. */
	public HorseManageMenu(HorseManagement plug) {
		super(plug);
	}

	@Override
	protected void setupInv() {
		inv = server.createInventory(new MenuHolder(title), 27, title);
		inv.setItem(11, unclaim);
		inv.setItem(13, select);
		inv.setItem(15, ride);
	}
	
	@Override
	public void onClick(InventoryClickEvent event) {
		ItemStack item = event.getCurrentItem();
		if (!item.equals(unclaim) && !item.equals(select) && !item.equals(ride)) return;
		Player player = (Player) event.getWhoClicked();
		player.closeInventory();
		UUID uuid = player.getUniqueId();
		Horse horse = horses.get(uuid);
		if (horse == null) {
			player.sendMessage("§cSomething bad happened!");
			HorseLogger.warn("Could not find ManageHorse for player " + player.getName() + "("
			+ player.getUniqueId() + ").");
			return;
		}
		horses.remove(uuid);
		if (item.equals(unclaim)) {
			YamlConfiguration horseData = plugin.getHorseData();
			String uuidStr = horse.getUniqueId().toString();
			horseData.set(uuidStr, null);
			Horse selectedHorse = plugin.getPlayerHorse(player);
			if (selectedHorse != null && 
					selectedHorse.getUniqueId().equals(horse.getUniqueId())) {
				plugin.removePlayerHorse(player);
			}
			player.sendMessage("§aHorse unclaimed successfully! To claim it again, use "
					+ "§b/horseown§a.");
		} else if (item.equals(select)) {
			plugin.setPlayerHorse(player, horse);
			player.sendMessage("§aHorse selected successfully! Use this with §b/rider§a, "
					+ "§b/horsename§a and §b/horsetp§a.");
		} else if (item.equals(ride)) {
			Util.ride(horse, player);
			player.sendMessage("§aWee-hoo!");
		}
	}

	/**
	 * Indicate that a player is managing a horse by adding it and its horse to the horse managing map.
	 * @param p Player that is managing.
	 * @param horse Horse being managed.
	 */
	public void setupPlayer(Player p, Horse horse) {
		horses.put(p.getUniqueId(), horse);
		show(p);
	}

	public static String getTitle() { return title; }

	@Override
	public String getMenuTitle() { return title; }
}

