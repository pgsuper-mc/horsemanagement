package io.github.PgBiel.HorseManagement.menus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import io.github.PgBiel.HorseManagement.menus.holders.MenuHolder;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import io.github.PgBiel.HorseManagement.HorseManagement;
import io.github.PgBiel.HorseManagement.util.Util;
import io.github.PgBiel.HorseManagement.HorseLogger;

public class ClaimHorseMenu extends Menu {
	
	private static Map<UUID, Horse> horses = new HashMap<UUID, Horse>();
	private static ItemStack item;
	protected static final String title = "§6§lClaim Horse";

	static {
		item = new ItemStack(Material.CHEST); // Make item
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName("§6Click me to claim!");
		item.setItemMeta(meta);
	}
	
	public ClaimHorseMenu(HorseManagement plug) {
		super(plug);
	}

	public static String getTitle() {
		return title;
	}
	public String getMenuTitle() { return title; }

	@Override
	protected void setupInv() {
		inv = server.createInventory(new MenuHolder(title), 27, title);
		inv.setItem(13, item);
	}
	
	@Override
	public void onClick(InventoryClickEvent event) {
		if (!event.getCurrentItem().equals(item)) return;
		YamlConfiguration horseData = plugin.getHorseData();
		Player player = (Player) event.getWhoClicked();
		UUID uuid = player.getUniqueId();
		Horse horse = horses.get(uuid);
		player.closeInventory();
		if (horse == null) {
			player.sendMessage("§cSomething bad happened!");
			HorseLogger.warn("Could not find ClaimHorse for player " + player.getName() + "("
					+ player.getUniqueId() + ").");
					return;
		}
		horses.remove(uuid);
		String uuidStr = horse.getUniqueId().toString();
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("owner", player.getUniqueId().toString());
		data.put("friends", new ArrayList<String>());
		horseData.createSection(uuidStr, data);
		player.sendMessage("§aSuccessfully claimed this horse! Open up the Horse Management "
				+ "menu again to manage.");
		Sound sound;
		if (Util.isOld()) {
			sound = Sound.valueOf("ORB_PICKUP"); // Compatibility!
		} else {
			sound = Sound.valueOf("ENTITY_EXPERIENCE_ORB_PICKUP");
		}
		player.playSound(player.getLocation(), sound, 90, 5);
	}
	
	public void setupPlayer(Player p, Horse horse) {
		horses.put(p.getUniqueId(), horse);
		show(p);
	}
}
