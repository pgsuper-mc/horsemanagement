package io.github.PgBiel.HorseManagement.menus;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import io.github.PgBiel.HorseManagement.HorseManagement;

public abstract class Menu implements OnClickable {

	/** The menu's inventory. */
	protected Inventory inv;
	/** The menu's title. */
	protected static String title;
	/** The plugin instance used. */
	protected HorseManagement plugin;
	/** The server the menu is in. */
	protected final Server server = Bukkit.getServer();

	/**
	 * Get the menu's title.
	 * @return The menu's title
	 */
	public static String getTitle() { return null; } // static - should be defined by subclasses
	/**
	 * Get the menu's title.
	 * @return The menu's title
	 */
	public abstract String getMenuTitle(); // non-static version

	public Menu(HorseManagement plug) {
		plugin = plug;
		setupInv();
	}

    /**
     * Get the inventory contents of this Menu.
     * @return The Menu's Inventory object.
     */
	public Inventory getInv() {
		return inv;
	}

	/**
	 * Show the menu to an online Player.
	 * @param p Player to show the menu to.
	 */
	public void show(Player p) {
		p.openInventory(inv);
	}

	/**
	 * Adds all the items to the inventory (GUI) and effectively creates it.
	 */
	protected abstract void setupInv();
}
