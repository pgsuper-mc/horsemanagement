package io.github.PgBiel.HorseManagement.menus.holders;

import org.bukkit.OfflinePlayer;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

public class MenuHolder implements InventoryHolder {
    protected Inventory inv;
    protected String id;

    public MenuHolder(String id) {
        this.id = id;
    }

    @Override
    public Inventory getInventory() {
        return inv;
    }

    public String getID() {
        return id;
    }
}
