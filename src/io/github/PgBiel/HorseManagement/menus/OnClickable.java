package io.github.PgBiel.HorseManagement.menus;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

/**
 * Anything that has an onClick event, for all Menus.
 */
public interface OnClickable {

    /**
     * Get this menu's inventory contents.
     * @return The Menu's inventory object.
     */
	public Inventory getInv();

	/**
	 * Method to run when this click event happens.
	 * @param event Event object.
	 */
	public void onClick(InventoryClickEvent event);
}
