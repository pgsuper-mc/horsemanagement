package io.github.PgBiel.HorseManagement;

import java.util.logging.Logger;

/**
 * Logging.
 */
public class HorseLogger {
	private static Logger logger;

	/**
	 * Log at INFO level.
	 * @param text Text to log.
	 */
	public static void info(String text) {
		logger.info(text);
	}

	/**
	 * Alias to HorseLogger#info.
	 * @param text Text to log.
	 */
	public static void debug(String text) {
		info(text);
	}

	/**
	 * Log at WARN level.
	 * @param text Text to log.
	 */
	public static void warn(String text) { 
		logger.warning(text);
	}

	/**
	 * Log at SEVERE level.
	 * @param text Text to log.
	 */
	public static void sev(String text) {
		logger.severe(text);
	}

	/**
	 * Set the original logger.
	 * @param lg Original logger to set; should only be set once in normal conditions.
	 */
	public static void setLogger(Logger lg) {
		if (logger == null) logger = lg;
	}
}
