package io.github.PgBiel.HorseManagement;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import io.github.PgBiel.HorseManagement.command.CommandHandler;
import io.github.PgBiel.HorseManagement.util.Util;

public class HorseManagement extends JavaPlugin {

	/** The plugin singleton instance. */
	private static HorseManagement plugin;

	/** Get the singleton instance of the plugin. */
	public static HorseManagement getPlugin() { return plugin; }

	/** Claimed horses' config. */
	private YamlConfiguration horseData;

	/** Selected horses' config. */
	private YamlConfiguration selectData;

	/** Temporary selected horses' config. */
	private Map<UUID, Horse> selectedHorses;

	/** List of people that have any kind of bypass. */
	private List<UUID> nonBypassers;

	/**
	 * Method executed when the plugin is enabled or server is started.
	 */
	@Override
	public void onEnable() {
		plugin = this;
		HorseLogger.setLogger(getLogger());
		HorseLogger.info("Setting up utilities...");
		Util.setPlugin(this);
		HorseLogger.info("Checking configuration...");
		configCheck();
		reload();
		selectedHorses = new HashMap<UUID, Horse>();
		nonBypassers = new ArrayList<UUID>();
		HorseLogger.info("Setting up command handler...");
		CommandHandler.setup(getServer(), this);
		HorseLogger.info("Setting up listener...");
		getServer().getPluginManager().registerEvents(new HorseListener(this), this);
		HorseLogger.info("Done.");
	}

	/**
	 * Method executed when the plugin is disabled or server is restarted/turned off.
	 */
	@Override
	public void onDisable() {
		HorseLogger.info("Saving config...");
		save();
		HorseLogger.info("Done.");
	}

	/**
	 * Method executed when a command is run by a player. Redirected to the CommandHandler.
	 * @param sender Who sent the command.
	 * @param cmd The Spigot Command instance.
	 * @param label The label of the command used.
	 * @param args The arguments provided.
	 * @return Whether (true) or not (false) this interaction with the Command was successful.
	 */
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player) && !(sender instanceof ConsoleCommandSender)) {
			return false;
		}
		
		CommandHandler.run(sender, cmd, label, args);
		return true;
	}

	/**
	 * Save the configuration before plugin unload.
	 * TODO: Move config part to HorseConfig.
	 */
	private void save() {
		try {
			// saveConfig(); -- I only commented this because we don't ever modify it
			String dataPath = Paths.get(getDataFolder().getAbsolutePath(), "data").toString();
			Path horsePath = Paths.get(dataPath, "horse.yml");
			Files.write(horsePath, horseData.saveToString().getBytes());
			
			Path selectPath = Paths.get(dataPath, "select.yml");
			Files.write(selectPath, selectData.saveToString().getBytes());
		} catch (Exception e) {
			HorseLogger.sev("There was an error saving config!");
			e.printStackTrace();
		}
	}

	/**
	 * Check if the configuration file exists and, if not, create it.
	 * TODO: Move to HorseConfig.
	 */
	private void configCheck() {
		try {
			if (!getDataFolder().exists()) {
				getDataFolder().mkdirs();
			}
			String pluginPath = getDataFolder().getAbsolutePath();
			if (Files.notExists(Paths.get(pluginPath, "config.yml"))) {
				saveDefaultConfig();
			}
			Path dataPath = Paths.get(pluginPath, "data");
			Files.createDirectories(dataPath);
			Path horsePath = Paths.get(dataPath.toString(), "horse.yml");
			if (Files.notExists(horsePath)) {
				Files.createFile(horsePath);
			}
			Path selectPath = Paths.get(dataPath.toString(), "select.yml");
			if (Files.notExists(selectPath)) {
				Files.createFile(selectPath);
			}
			horseData = new YamlConfiguration();
			horseData.load(new File(horsePath.toString()));
			
			selectData = new YamlConfiguration();
			selectData.load(new File(selectPath.toString()));
		} catch (Exception e) {
			HorseLogger.sev("Error occurred at checking configuration!");
			e.printStackTrace();
		}
	}

	/**
	 * Reload configuration.
	 * TODO: Move to HorseConfig; initialize plugin reference and do plugin.reloadConfig()
	 */
	public void reload() {
		try {
			reloadConfig();
		} catch (Exception e) {
			HorseLogger.sev("There was an error loading configuration!");
			e.printStackTrace();
		}
	}

	/**
	 * Get an entity by its UUID.
	 * @param uniqueId The UUID of the entity.
	 * @return The entity, if found; otherwise, null.
	 */
	public Entity getEntityByUniqueId(UUID uniqueId){ // TODO: Move to Util; Reduce this to one loop with world.getLivingEntities()
	    for (World world : Bukkit.getWorlds()) {
	        for (Chunk chunk : world.getLoadedChunks()) {
	            for (Entity entity : chunk.getEntities()) {
	                if (entity.getUniqueId().equals(uniqueId))
	                    return entity;
	            }
	        }
	    }

	    return null;
	}

	// TODO: move all of this crap to HorseConfig

	public YamlConfiguration getHorseData() {
		return horseData;
	}
	
	public YamlConfiguration getSelectData() {
		return selectData;
	}
	
	// Selected horses manipulation below

	public Horse getPlayerHorse(Player p) {
		return getPlayerHorse(p.getUniqueId());
	}

	/**
	 * Get a player's selected horse.
	 * @param pUUID The player's UUID.
	 * @return The horse, if any is selected.
	 */
	public Horse getPlayerHorse(UUID pUUID) {
		String uuid = pUUID.toString();
		Horse horse = selectedHorses.get(pUUID);
		if (horse == null) {
			String uid = selectData.getString(uuid);
			if (uid == null) return null;
			Entity ent = getEntityByUniqueId(UUID.fromString(uid));
			if (ent == null || !(ent instanceof Horse)) {
				selectData.set(uuid, null);
				return null;
			}
			return (Horse) ent;
		}
		return horse;
	}

	/**
	 * Set a player's horse selection. (By class)
	 * @param p OfflinePlayer or Player
	 * @param horse Horse to set.
	 */
	public void setPlayerHorse(OfflinePlayer p, Horse horse) {
		setPlayerHorse(p.getUniqueId(), horse);
	}

	/**
	 * Set a player's horse selection.
	 * @param pUUID UUID of player to use.
	 * @param horse Horse to set selection to.
	 */
	public void setPlayerHorse(UUID pUUID, Horse horse) {
		selectedHorses.put(pUUID, horse);
		selectData.set(pUUID.toString(), horse.getUniqueId().toString());
	}

	/**
	 * Remove a player's horse selection. (By class)
	 * @param p OfflinePlayer to remove selection from
	 */
	public void removePlayerHorse(OfflinePlayer p) {
		removePlayerHorse(p.getUniqueId());
	}

	/**
	 * Remove a player's horse selection.
	 * @param pUUID UUID of player to use.
	 */
	public void removePlayerHorse(UUID pUUID) {
		selectedHorses.remove(pUUID);
		selectData.set(pUUID.toString(), null);
	}

	/**
	 * Add a player to bypasser list (immunity to claimed horses' restrictions, based on the player's perms).
	 * @param p Player to add.
	 */
	public void addPlayerBypass(OfflinePlayer p) {
		UUID uuid = p.getUniqueId();
		if (!nonBypassers.contains(uuid)) nonBypassers.add(uuid);
	}

	/**
	 * Remove a player from bypasser list (immunity to claimed horses' restrictions, based on the player's perms).
	 * @param p Player to remove.
	 */
	public void remPlayerBypass(OfflinePlayer p) {
		UUID uuid = p.getUniqueId();
		nonBypassers.remove(uuid);
	}

	/**
	 * See whether a player is a bypasser (immunity to claimed horses' restrictions, based on the player's perms).
	 * @param p Player to check.
	 * @return If player is bypasser (true) or not (false).
	 */
	public boolean getPlayerBypass(OfflinePlayer p) {
		return nonBypassers.contains(p.getUniqueId());
	}
}
