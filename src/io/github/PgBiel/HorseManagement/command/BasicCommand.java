package io.github.PgBiel.HorseManagement.command;

import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import io.github.PgBiel.HorseManagement.HorseManagement;

/**
 * A command's structure.
 */
public abstract class BasicCommand {
	protected CommandSender sender;
	protected Command cmd;
	protected Player player;
	protected World world;
	protected Server server;
	protected FileConfiguration config;
	protected HorseManagement plugin;
	protected String label;
	protected String[] args;

	protected String name; // should be set by subclass
	protected String[] aliases; // could be set
	protected boolean acceptConsole; // optional subclass setting
	protected boolean usesPermissions; // if has permission
	protected String[] permissions; // permission(s)

	public BasicCommand() {
		this.name = "";
		this.aliases = new String[]{};
		this.acceptConsole = true;
		this.usesPermissions = false;
	}
	public BasicCommand(String name, String[] aliases, boolean acceptConsole, String... permissions) {
		this.name = name;
		this.aliases = aliases == null ? new String[]{} : aliases;
		this.acceptConsole = acceptConsole;

		boolean usesPermissions = permissions != null && permissions.length > 0;
		this.usesPermissions = usesPermissions;
		if (usesPermissions) this.setPermissions(permissions);
	}

	/**
	 * Where all of the command's structure is set by the CommandHandler.
	 * @param server Server the command is situated in.
	 * @param config Plugin's config (TODO: Use HorseConfig instead.)
	 * @param plugin Plugin itself.
	 * @param sender Who executed the command.
	 * @param cmd The Spigot Command.
	 * @param label The command label used.
	 * @param args The command arguments provided, in form of string array.
	 */
	protected void setup(Server server, FileConfiguration config, HorseManagement plugin, 
			CommandSender sender, Command cmd, String label, String[] args) {
		this.sender = sender;
		if (sender instanceof Player) {
			this.player = (Player) sender;
			this.world = this.player.getWorld();
		} else {
			this.player = null;
			this.world = null;
		}
		if (server != null) this.server = server;
		this.config = config;
		if (plugin != null) this.plugin = plugin;
		this.cmd = cmd;
		this.label = label;
		this.args = args;
	}
	
	public String getLabel() {
		return label;
	}
	
	public Server getServer() {
		return server;
	}
	
	public FileConfiguration getConfig() {
		return config;
	}
	
	public String[] getArgs() {
		return args;
	}
	
	public String[] getAliases() {
		return aliases;
	}
	
	public String getName() {
		return name;
	}
	
	public String[] getPermissions() {
		return permissions;
	}
	
	protected void setPermissions(String... perms) {
		permissions = perms;
	}
	
	protected void setAliases(String... alls) {
		aliases = alls;
	}

	/**
	 * Get either acceptConsole or usesPermissions. // TODO: Turn both into separate functions
	 * @param name "acceptConsole" or "usesPermissions"
	 * @return The option's value.
	 * @throws IllegalArgumentException If the name is not provided or provided but with invalid contents.
	 */
	public boolean getOption(String name) throws IllegalArgumentException {
		if (name.equalsIgnoreCase("acceptConsole")) {
			return acceptConsole;
		} else if (name.equalsIgnoreCase("usesPermissions")) {
			return usesPermissions;
		}
		throw new IllegalArgumentException("Invalid option!");
	}

	/**
	 * Makes the command work.
	 */
	public abstract void run();
}