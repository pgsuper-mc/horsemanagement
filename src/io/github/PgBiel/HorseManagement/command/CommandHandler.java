package io.github.PgBiel.HorseManagement.command;

import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import io.github.PgBiel.HorseManagement.HorseManagement;
import io.github.PgBiel.HorseManagement.command.BasicCommand;
import io.github.PgBiel.HorseManagement.command.commands.HorseReloadCommand;
import io.github.PgBiel.HorseManagement.command.commands.HorseBypassCommand;
import io.github.PgBiel.HorseManagement.command.commands.HorseNameCommand;
import io.github.PgBiel.HorseManagement.command.commands.HorseOwnCommand;
import io.github.PgBiel.HorseManagement.command.commands.HorseRiderCommand;
import io.github.PgBiel.HorseManagement.command.commands.HorseTPCommand;
import io.github.PgBiel.HorseManagement.util.Util;

/** Handles the onCommand listener. */
public class CommandHandler {
	/** Command list. */
	private static final BasicCommand[] commands = { new HorseNameCommand(), new HorseOwnCommand(), 
			new HorseRiderCommand(), new HorseTPCommand(), new HorseReloadCommand(),
			new HorseBypassCommand() };
	/** Server the handler is in. */
	private static Server server;
	/** The plugin instance being used. */
	private static HorseManagement plugin;

	/**
	 * Run the commands.
	 * @param sender Who sent the command.
	 * @param cmd The Spigot Command used.
	 * @param label The command's label.
	 * @param args The arguments provided, in form of a String array.
	 */
	public static void run(CommandSender sender, Command cmd, String label, String[] args) {
		String name = cmd.getName();
		for (BasicCommand command : commands) {
			if (
					command.getName() != null && (
							command.getName().equalsIgnoreCase(name)
							|| (command.getAliases().length > 0 
									&& Util.arrayContainsIns(command.getAliases(), name))
					)
			) {
				command.setup(server, plugin.getConfig(), plugin, sender, cmd, label, args);
				if (command.getOption("acceptConsole")) {
					command.run();
				} else if (sender instanceof Player) {
					command.run();
				}
				break;
			}
		}
	}

	/**
	 * Set up this class.
	 * @param s The server this CommandHandler operates in.
	 * @param plug The plugin.
	 */
	public static void setup(Server s, HorseManagement plug) {
		if (server == null) server = s;
		if (plugin == null) plugin = plug;
	}
}
