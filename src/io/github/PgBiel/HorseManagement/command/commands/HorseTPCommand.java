package io.github.PgBiel.HorseManagement.command.commands;

import org.bukkit.Sound;
import org.bukkit.entity.Horse;

import io.github.PgBiel.HorseManagement.command.BasicCommand;
import io.github.PgBiel.HorseManagement.util.Util;

public class HorseTPCommand extends BasicCommand {
	
	public HorseTPCommand() {
		super("horsetp", null, false, "horsemng.tp");
	}
	
	@Override
	public void run() {
		Horse horse = plugin.getPlayerHorse(player);
		if (horse == null) {
			player.sendMessage("§cYou have no horse selected! Open the horse management menu for "
					+ "a horse you own and select §7Select Horse§c to use this command to "
					+ "teleport your horse to you.");
			Sound sound;
			if (Util.isOld()) { // this is necessary for the sake of compatibility, unfortunately
				sound = Sound.valueOf("VILLAGER_NO");
			} else {
				sound = Sound.valueOf("ENTITY_VILLAGER_NO");
			}
			player.playSound(player.getLocation(), sound, 90, 5);
			return;
		}
		horse.teleport(player);
		player.sendMessage("§aSuccessfully teleported your horse to you!");
	}
}
