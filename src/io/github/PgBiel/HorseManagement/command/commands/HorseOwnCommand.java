package io.github.PgBiel.HorseManagement.command.commands;

import org.bukkit.Sound;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import io.github.PgBiel.HorseManagement.command.BasicCommand;
import io.github.PgBiel.HorseManagement.util.Util;

public class HorseOwnCommand extends BasicCommand {
	
	public HorseOwnCommand() {
		super("horseown", null, false, "horsemng.own");
	}
	
	@Override
	public void run() {
		PlayerInventory inv = player.getInventory();
		ItemStack book = Util.getHorseOwnItem();
		inv.remove(book);
		inv.addItem(book);
		String[] msg = {
				"§aYou have been given the horse management item!",
				"§aHit a tamed horse while holding it to open the menu."
		};
		player.sendMessage(msg);
		Sound sound;
		if (Util.isOld()) {
			sound = Sound.valueOf("ORB_PICKUP");
		} else {
			sound = Sound.valueOf("ENTITY_EXPERIENCE_ORB_PICKUP");
		}
		player.playSound(player.getLocation(), sound, 90, 5);
	}
}
