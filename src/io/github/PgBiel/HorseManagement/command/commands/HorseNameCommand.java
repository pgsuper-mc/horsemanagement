package io.github.PgBiel.HorseManagement.command.commands;

import org.bukkit.Sound;
import org.bukkit.entity.Horse;

import io.github.PgBiel.HorseManagement.command.BasicCommand;
import io.github.PgBiel.HorseManagement.util.Util;

public class HorseNameCommand extends BasicCommand {
	
	public HorseNameCommand() {
		super("horsename", null, false, "horsemng.name");
	}
	
	@Override
	public void run() {
		Horse horse = plugin.getPlayerHorse(player);
		if (horse == null) {
			player.sendMessage("§cYou have no horse selected! Open the horse management menu for "
					+ "a horse you own and select §7Select Horse§c to use this command to "
					+ "change your horse's name.");
			Sound sound;
			if (Util.isOld()) {
				sound = Sound.valueOf("VILLAGER_NO");
			} else {
				sound = Sound.valueOf("ENTITY_VILLAGER_NO");
			}
			player.playSound(player.getLocation(), sound, 90, 5);
			return;
		}
		if (args.length < 1) {
			String name = horse.getCustomName();
			player.sendMessage("§aYou selected " + (name == null || name == "" ? "a horse with "
					+ "no name" : "the horse named §e§o" + name) + "§a. Type §b/horsename <name> "
							+ "to change your horse's name to <name>. §7NOTE: Color codes (with "
							+ "& + digit) accepted!");
			return;
		}
		String newName = String.join(" ", args);
		newName = Util.processColors(newName);
		horse.setCustomName(newName);
		player.sendMessage("§aHorse renamed successfully!");
	}
}
