package io.github.PgBiel.HorseManagement.command.commands;

import io.github.PgBiel.HorseManagement.command.BasicCommand;

public class HorseBypassCommand extends BasicCommand {

	public HorseBypassCommand() {
		super("horsebypass", null, false, "horsemng.bypass.command");
	}
	
	@Override
	public void run() {
		if (plugin.getPlayerBypass(player)) {
			plugin.remPlayerBypass(player);
			player.sendMessage("§aTurned bypassing limitations on!");
		} else {
			plugin.addPlayerBypass(player);
			player.sendMessage("§aTurned bypassing limitations off!");
		}
	}
	
}
