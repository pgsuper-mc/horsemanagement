package io.github.PgBiel.HorseManagement.command.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Horse;

import io.github.PgBiel.HorseManagement.command.BasicCommand;
import io.github.PgBiel.HorseManagement.util.Util;

/**
 * Command for giving people access to ride your horse.
 */
public class HorseRiderCommand extends BasicCommand {
	
	public HorseRiderCommand() {
		super("rider", new String[]{ "horserider" }, false, "horsemng.rider");
	}

	@Override
	public void run() {
		Horse horse = plugin.getPlayerHorse(player);
		if (horse == null) {
			player.sendMessage("§cYou have no horse selected! Open the horse management menu for "
					+ "a horse you own and select §7Select Horse§c to use this command to "
					+ "add a rider.");
			Sound sound;
			if (Util.isOld()) {
				sound = Sound.valueOf("VILLAGER_NO");
			} else {
				sound = Sound.valueOf("ENTITY_VILLAGER_NO");
			}
			player.playSound(player.getLocation(), sound, 90, 5);
			return;
		}
		YamlConfiguration horseData = plugin.getHorseData();
		ConfigurationSection section = horseData.getConfigurationSection(horse.getUniqueId()
				.toString());
		List<String> friends = section.getStringList("friends");
		if (friends == null) {
			section.set("friends", new ArrayList<String>());
			friends = section.getStringList("friends");
		}
		if (args.length < 1) {
			String name = horse.getCustomName();
			player.sendMessage("§aYou selected " + (name == null || name == "" ? "a horse with "
					+ "no name" : "the horse named §e§o" + name) + "§a. Type §b/rider add <name> "
							+ "§ato let <name> ride your horse; §b/rider remove <name>§a to not "
							+ "let them; §b/rider clear§a to remove all and §b/rider list§a to "
							+ "list them.");
		} else if (args[0].equalsIgnoreCase("add")) {
			if (args.length < 2) {
				player.sendMessage("§cPlease specify who to add!");
				return;
			}
			String name = args[1];
			for (OfflinePlayer offp : server.getOfflinePlayers()) {
				if (offp.getName().equalsIgnoreCase(name)) {
					String uuidStr = offp.getUniqueId().toString();
					if (friends.contains(uuidStr)) {
						player.sendMessage("§cThat player already can ride your horse!");
						return;
					}
					friends.add(uuidStr);
					section.set("friends", friends);
					player.sendMessage("§aSuccessfully added player §e" + offp.getName() + "§a to "
							+ "list of allowed riders!");
					return;
				}
			}
			player.sendMessage("§cPlayer not found!");
		} else if (args[0].equalsIgnoreCase("remove")) {
			if (args.length < 2) {
				player.sendMessage("§cPlease specify who to remove!");
				return;
			}
			String name = args[1];
			for (OfflinePlayer offp : server.getOfflinePlayers()) {
				if (offp.getName().equalsIgnoreCase(name)) {
					String uuidStr = offp.getUniqueId().toString();
					if (!friends.contains(uuidStr)) {
						player.sendMessage("§cThat player already cannot ride your horse!");
						return;
					}
					friends.remove(uuidStr);
					section.set("friends", friends);
					player.sendMessage("§aSuccessfully removed player §e" + offp.getName() + "§a "
							+ "from list of allowed riders!");
					return;
				}
			}
			player.sendMessage("§cPlayer not found!");
		} else if (args[0].equalsIgnoreCase("clear")) {
			if (friends.size() < 1) {
				player.sendMessage("§cYou already have nobody that can ride your horse!");
				return;
			}
			friends.clear();
			section.set("friends", friends);
			player.sendMessage("§aSuccessfully cleared your list of allowed riders!");
		} else if (args[0].equalsIgnoreCase("list")) {
			if (friends.size() < 1) {
				player.sendMessage("§aYou haven't let any players ride your horse!");
				return;
			}
			int page = 1;
			if (args.length > 1) {
				try {
					page = Integer.parseInt(args[1]);
				} catch (NumberFormatException e) {
					player.sendMessage("§cInvalid page number!");
					return;
				}
			}
			if (page < 1) {
				player.sendMessage("§cThe page must be a positive and non-zero number!");
				return;
			}
			List<List<String>> pages = new ArrayList<List<String>>();
			pages.add(new ArrayList<String>());
			int index = 0;
			for (String friend : friends) {
				OfflinePlayer offp = server.getOfflinePlayer(UUID.fromString(friend));
				List<String> currList = pages.get(index);
				if (offp.hasPlayedBefore()) {
					currList.add(ChatColor.YELLOW + offp.getName());
				} else {
					currList.add(ChatColor.YELLOW + friend + ChatColor.GREEN + "(did not play "
							+ "before so name is unknown)");
				}
				if (currList.size() >= 10) {
					pages.add(new ArrayList<String>());
					index++;
				}
			}
			if (page > pages.size()) {
				player.sendMessage("§cPage number too high! (Max number: §7" + pages.size()
				+ "§c)");
				return;
			}
			List<String> pageList = pages.get(page - 1);
			pageList.add(0, "§6Rider List - Page§c " + page + "§6 of §c" + pages.size());
			if (pages.size() > 1) {
				pageList.add("§7§oNavigate with §c§o/rider list <page>§7§o.");
			}
			player.sendMessage(pageList.toArray(new String[0]));
		} else {
			player.sendMessage("§cInvalid parameter!");
		}
	}
}
