package io.github.PgBiel.HorseManagement.command.commands;

import io.github.PgBiel.HorseManagement.HorseLogger;
import io.github.PgBiel.HorseManagement.command.BasicCommand;

public class HorseReloadCommand extends BasicCommand {

	public HorseReloadCommand() {
		super("horsereload", null, true, "horsemng.reload");
	}
	
	@Override
	public void run() {
		sender.sendMessage("§6Reloading config...");
		try {
			plugin.reload();
		} catch (Exception e) {
			sender.sendMessage("§cCould not reload config!");
			HorseLogger.sev("Error at reloading config!");
			e.printStackTrace();
		}
		sender.sendMessage("§aDone.");
	}
}
